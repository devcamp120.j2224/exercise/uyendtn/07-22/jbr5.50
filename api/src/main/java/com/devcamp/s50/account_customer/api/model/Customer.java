package com.devcamp.s50.account_customer.api.model;

public class Customer {
    int id;
    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getId() {
        return id;
    }
    String name;
    public String getName() {
        return name;
    }
    int discount;
    public int getDiscount() {
        return discount;
    }
    public void setDiscount(int discount) {
        this.discount = discount;
    }
    public Customer() {
    }
    public Customer(int id, String name, int discount) {
        this.id = id;
        this.name = name;
        this.discount = discount;
    }
    public String toString() {
        return "Customer[" + name + " (" + id + "%)";
    }
   

}
