package com.devcamp.s50.account_customer.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.account_customer.api.model.Account;
import com.devcamp.s50.account_customer.api.service.AccountService;

@RestController
@RequestMapping("/")
@CrossOrigin(value="*",maxAge = -1)
public class AccountController {
    @Autowired
    private AccountService accountService;
    @GetMapping("/accounts")
    public List<Account> getAccounts() {
        List<Account> accountList = accountService.getAccountList();
        return accountList;
    }
}
