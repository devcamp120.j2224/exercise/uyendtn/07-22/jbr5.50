package com.devcamp.s50.account_customer.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.s50.account_customer.api.model.Account;

@Service
public class AccountService {
    @Autowired
    private CustomerService customerService;

    Account account1 = new Account(1, 10000);
    Account account2 = new Account(2,20000);
    Account account3 = new Account(3,30000);

    public List<Account> getAccountList() {
        List<Account> accountList = new ArrayList<Account>();
        account1.setCustomer(customerService.customer1);
        account2.setCustomer(customerService.customer2);
        account3.setCustomer(customerService.customer3);
        accountList.add(account1);
        accountList.add(account2);
        accountList.add(account3);
        return accountList;
    }
}
