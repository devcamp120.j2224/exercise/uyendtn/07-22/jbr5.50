package com.devcamp.s50.account_customer.api.model;

public class Account {
    int id;
    Customer customer;
    double balance = 0.0;

    public int getId() {
        return id;
    }
    
    public Customer getCustomer() {
        return customer;
    }
    
    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }
    public Account(int id, Customer customer) {
        this.id = id;
        this.customer = customer;
    }
    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }
    public String toString() {
        return customer.toString() + "balance= " + Math.round(balance * 100.0) / 100.0;
    }
    public String getCustomerName() {
       return customer.getName();
    }
    public double deposit(double amount) {
        return balance += amount;
    }
    public double withdraw(double amount) {
        if (amount <= balance){
            balance -= amount;
        } else {
            System.out.println("amount withdraw exceed current balance ");
        }
        return balance;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Account() {
    }

    public Account(int id, double balance) {
        this.id = id;
        this.balance = balance;
    }

}
